
import telnetlib
from scripts.datastore import Command, Preferences
from pydispatch import dispatcher
from threading import Thread

class DGXShellJobs(Thread):

    def __init__(self, queue, prefs: Preferences):
        Thread.__init__(self)
        self.queue = queue
        self.shutdown = False
        self.prefs = prefs
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            text_command = self.queue.get()
            self.dgx_shell_command(text_command.command)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def establish_telnet(self, ip_address):
        """Creates the telnet instance"""
        telnet_session = telnetlib.Telnet(ip_address, 23, 5)
        telnet_session.set_option_negotiation_callback(self.call_back)
        return telnet_session

    def call_back(self, sock, cmd, opt):
        """ Turns on server side echoing"""
        if opt == telnetlib.ECHO and cmd in (telnetlib.WILL, telnetlib.WONT):
            sock.sendall(telnetlib.IAC + telnetlib.DO + telnetlib.ECHO)

    def check_for_login(self, telnet_session):
        """Checks if we need to login and returns the header"""
        intro = telnet_session.read_some()
        if intro.split()[0] == b'Login':
            need_to_login = True
        else:
            need_to_login = False
        return need_to_login, intro.split()

    def login(self, telnet_session, username, password):
        """Log in when required"""
        try:
            telnet_session.write(f'{username}\r'.encode())
            telnet_session.read_until(b'Password :')
            telnet_session.write(f'{password}\r'.encode())
            intro = telnet_session.read_until(b'>', 5).split()[1:]
            if intro[0] != b'Welcome':
                return False
            return intro
        except Exception as error:
            print('during login: ', error)
            return False

    def dgx_shell_command(self, command: Command):
        """"""
        for ip_address in command.ip_addresses:
            try:
                telnet_session = self.establish_telnet(ip_address)
                # need_to_login, intro = self.check_for_login(telnet_session)
                # if need_to_login:
                #     intro = self.login(telnet_session, self.prefs.netlinx_username, self.prefs.get_password())
                #     if intro is False:
                #         dispatcher.send(signal="Status Update", message=f"Unable to login to dgx {ip_address}")
                #         return
                telnet_session.write(b'\r\n')
                telnet_session.read_until(b'DGX_SHELL>', 5)
                if command.dgx_su:
                    dispatcher.send(signal="Status Update", message=f"DGX Elevating Privileges")
                    telnet_session.write(b'su\r\n')
                    telnet_session.read_until(b'username:', 5)
                    telnet_session.write(b'enova\r\n')
                    telnet_session.read_until(b'password:', 5)
                    telnet_session.write(b'12amx34\r\n')
                    telnet_session.read_until(b'DGX_SHELL>', 5)
                
                telnet_session.write(command.command.encode())
                telnet_session.read_until(b'DGX_SHELL>', 5)
                dispatcher.send(signal="Status Update", message=f"Sent DGX command {command.command.strip()} to {ip_address}")
                telnet_session.close()
            except Exception as error:
                dispatcher.send(signal="Status Update", message=f"DGX error: {error}")


