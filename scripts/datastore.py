from dataclasses import dataclass, field
from cryptography.fernet import Fernet
import os

ENCRYPTION_KEY = b'pXGkrbOJL4gEq-qfwQxMBeCBUEPo_KewDrSHQfIp-_Q='
cipher_suite = Fernet(ENCRYPTION_KEY)

@dataclass
class Command:

    system: str = None
    command: str = None
    wait_time: int = None
    dgx_su: bool = False
    expected_text_list: list = field(default_factory=list)
    ip_addresses: list = field(default_factory=list)

    def __str__(self):
        return str(self.__dict__)



@dataclass
class Action:

    """Text to command
    The idea here is to have a text formated command that can be translated to actions.
    Format is comma seperated
    First field is type of system (not case sensitive)
    Netlinx, SVSi, or Camera

    For Netlinx
    Netlinx|<ip_address>|<on or off>|<[d:p:s,c]>
    Example
    netlinx|192.168.1.2|on|[5002:1:2,1]

    For SVSi
    SVSi|<ip_address_list>|<api_command>
    Example
    svsi|192.168.1.2,192.168.1.3|seta 102

    For Camera
        - wait time to 0 to not wait
        - expected result is always a list ie Code 1,2,3
    Camera|QR|<wait time>|<expected result>
    Example
    camera|QR|10|['Code 1']

    For Wait
    Wait|<seconds to wait>
    Example
    wait|10
    """

    enabled: bool = True
    text: str = ''
    panel: str = None
    command: str = None
    valid: bool = False
    single_run: bool = False

    #Philips
    name: str = ''
    msg_size: str = ''
    ctrl: str = ''
    group: str = ''
    data:  list = field(default_factory=list)
    check_sum: str = ''
    ip_address: str = ''



    def update_text(self, philips=False):
        self.command = None
        self.valid = False
        if not philips:
            try:
                com_list = self.text.split('|')
                system = com_list[0].lower()
                if system == 'svsi':
                    self.command = Command(system=system,
                                        ip_addresses=com_list[1].split(','),
                                        command=com_list[2],)
                    self.valid = True
                    return
                elif system == 'wait':
                    self.command = Command(system=system,
                                        command=int(com_list[1]))
                    self.valid = True
                    return
                elif system == 'camera':
                    action = com_list[1].lower()
                    if action == "qr":
                        self.command = Command(system=system,
                                            command=action,
                                            wait_time=int(com_list[2]),
                                            expected_text_list=com_list[3].split(','))
                        self.valid = True
                        return
                    if action == "show_camera":
                        self.command = Command(system=system,
                                            command=action)
                        self.valid = True
                        return
                elif system == 'netlinx':
                    if com_list[3]:
                        self.command = Command(system=system,
                                            ip_addresses=com_list[1].split(','),
                                            command=f"{com_list[2]} {com_list[3]}\r")
                    else:
                        self.command = Command(system=system,
                                            ip_addresses=com_list[1].split(','),
                                            command=f"{com_list[2]}\r")
                    self.valid = True
                    return
                elif system == 'dgx':
                    if com_list[3]:
                        su = True
                    else:
                        su = False
                    self.command = Command(system=system,
                                           ip_addresses=com_list[1].split(','),
                                           command=f"{com_list[2]}\r\n",
                                           dgx_su=su)
                    self.valid = True
                    return
                elif system == 'sound':
                    self.command = Command(system=system,
                                        command=com_list[1])
                    if os.path.exists(os.path.join(os.getcwd(), self.command.command)):
                        self.valid = True
            except Exception as error:
                print(com_list)
                print('had an error parsing command: ', error)
                return
        else:
            try:
                # We need to do the following
                # Cal msg size
                # Cal check sum
                if self.ctrl != '' and self.data != []:
                    self.valid = True
                    self.msg_size = str(len(self.data) + 4)
                    checksum = 0
                    for piece in [self.msg_size, self.ctrl, self.group] + self.data:
                        checksum = checksum ^ int(piece, 16)
                    self.check_sum = hex(checksum)[2:].zfill(2).upper()
                    
                    command_str = " ".join([self.msg_size.zfill(2).upper(), self.ctrl.zfill(2).upper(), self.group.zfill(2).upper(), " ".join(self.data), self.check_sum.zfill(2).upper()])
                    self.command = Command(command=command_str, ip_addresses=[self.ip_address])
                    return
            except Exception as error:
                self.valid = False
                print('had an error parsing command: ', error)
                return

    def __str__(self):
        return str(self.__dict__)


@dataclass
class Preferences:

    camera: int = 0
    action_list: list = field(default_factory=list)
    send_email_on_error: bool = False
    smtp_server: str = ''
    smtp_port: int = 465
    sender_email: str = ''
    receiver_email: str = ''
    smtp_password: str = b'gAAAAABg7lVZhmK1gj_HQttrKAoyIFB-2epmswJKXrEDV-2UVmTMZxrLuCv8fXA_O54jdtoD_d1IBZBIMs9pKH-qCHy8McpB-A=='  # password
    num_loops_txt: str = '1'
    repeat_chk: bool = False
    stop_on_error_chk: bool = False
    play_sounds: bool = False
    append_cr: bool = True
    netlinx_username: str = 'administrator'
    netlinx_password: str = b'gAAAAABg7lVZhmK1gj_HQttrKAoyIFB-2epmswJKXrEDV-2UVmTMZxrLuCv8fXA_O54jdtoD_d1IBZBIMs9pKH-qCHy8McpB-A=='  # password
    
    philips_testing: bool = False
    ip_address: str = ''

    def set_password(self, password):
        self.netlinx_password = cipher_suite.encrypt(password.encode())

    def get_password(self):
        try:
            return cipher_suite.decrypt(self.netlinx_password).decode()
        except Exception:
            return ''

    def set_smtp_password(self, password):
        self.smtp_password = cipher_suite.encrypt(password.encode())

    def get_smtp_password(self):
        try:
            return cipher_suite.decrypt(self.smtp_password).decode()
        except Exception:
            return ''
