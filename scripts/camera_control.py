from pydispatch import dispatcher
from threading import Thread
import cv2
import datetime
from pyzbar import pyzbar
from collections import Counter


class CameraJobs(Thread):

    def __init__(self, queue, prefs):

        self.prefs = prefs
        self.queue = queue
        self.shutdown = False
        self.checking_webcam = True
        self.abort = False
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        dispatcher.connect(self.abort_signal,
                           signal="Abort")
        dispatcher.connect(self.checking_signal,
                           signal="Checking")
        Thread.__init__(self)

    def run(self):
        self.cam = cv2.VideoCapture(self.prefs.camera)
        while not self.shutdown:
            # gets the job from the queue
            action = self.queue.get()
            # print(action.command)
            getattr(self, action.command.command)(action)

            # send a signal to the queue that the job is done
            self.queue.task_done()
        self.cam.release()

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def abort_signal(self, value=True):
        self.abort = value

    def checking_signal(self, value):
        self.checking_webcam = value

    def get_photo(self, action):
        """Get photo from camera"""
        ret_val, img = self.take_photo()
        dispatcher.send(signal="New Image", photo=img, action=action, ret_val=ret_val)

    def qr(self, action):
        """Waits for a qr code to come into view"""
        finish_time = datetime.datetime.now() + datetime.timedelta(seconds=action.command.wait_time)
        my_barcodes = []
        while not self.abort:
            ret_val, img = self.take_photo()
            if not ret_val:
                dispatcher.send(signal="Print Log", log="Unable to open camera")
                return
            barcodes, frame = self.read_barcodes(img)
            dispatcher.send(signal="Preview", frame=frame, width=self.cam.get(cv2.CAP_PROP_FRAME_WIDTH), height=self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
            if len(barcodes) >= len(action.command.expected_text_list):
                my_barcodes = []
                for barcode in barcodes:
                    my_barcodes.append(barcode.data.decode())
                # we have found all bar codes
                # Check if they match
                found_all = True
                for text in action.command.expected_text_list:
                    if text not in my_barcodes:
                        found_all = False
                if found_all:
                    self.save_image(action, frame=frame, my_barcodes=my_barcodes)
                    return

            if datetime.datetime.now() >= finish_time:
                break
            time_left = finish_time - datetime.datetime.now()
            dispatcher.send(signal="Timer", data=str(time_left.seconds))
        if not self.abort:
            self.save_image(action, frame=frame, my_barcodes=my_barcodes)

    def read_barcodes(self, frame):
        barcodes = pyzbar.decode(frame)
        for barcode in barcodes:
            x, y, w, h = barcode.rect

            barcode_info = barcode.data.decode('utf-8')
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, barcode_info, (x + 6, y - 6), font, 2.0, (255, 255, 255), 1)
        return barcodes, frame

    def show_camera(self, action):
        """Show the camera for alignment"""
        try:
            while self.checking_webcam:

                ret_val, img = self.take_photo()
                if not ret_val:
                    dispatcher.send(signal="Print Log", log="Unable to open camera")
                    return
                barcodes, frame = self.read_barcodes(img)
                dispatcher.send(signal="Preview", frame=frame, width=self.cam.get(cv2.CAP_PROP_FRAME_WIDTH), height=self.cam.get(cv2.CAP_PROP_FRAME_HEIGHT))
        except Exception as error:
            dispatcher.send(signal="Print Log", log=f"show camera exception: {error}")
            pass

    def take_photo(self):
        """Takes a photo"""
        ret_val = False
        img = None
        count = 0
        while not ret_val:
            count += 1
            ret_val, img = self.cam.read()
            if count > 10:
                break
        if not ret_val:
            dispatcher.send(signal="Print Log", log=f"Unable to access camera, check if another process is using it")
        return ret_val, img

    def save_image(self, action, frame, my_barcodes):
        now = datetime.datetime.now().strftime('%d%m%y%H%M%S')
        if len(action.command.expected_text_list) == len(my_barcodes) and Counter(action.command.expected_text_list) == Counter(my_barcodes):
            # Success
            dispatcher.send(signal="Save Photo", frame=frame, successful=True, now=now, action=action)
            dispatcher.send(signal="Print Log", log=f'Successfully found {len(my_barcodes)} qr code(s) containing {action.command.expected_text_list}')
        else:
            # Failure
            dispatcher.send(signal="Save Photo", frame=frame, successful=False, now=now, action=action)
            dispatcher.send(signal="Print Log",
                            log=f'Failed. I found {len(my_barcodes)} qr codes containing {my_barcodes} this does not match the required {len(action.command.expected_text_list)} qr codes containing {action.command.expected_text_list}')

