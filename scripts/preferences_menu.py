import auto_test_gui
from auto_test import Auto_Test_Frame


class PreferencesConfig(auto_test_gui.PreferencesMenu):
    """Sets the PreferencesMenu """

    def __init__(self, parent: Auto_Test_Frame):
        auto_test_gui.PreferencesMenu.__init__(self, parent)

        self.parent = parent
        self.prefs = self.parent.prefs
        self.camera_txt.SetValue(str(self.prefs.camera))
        self.philips_chk.SetValue(self.prefs.philips_testing)
        self.play_sounds_chk.SetValue(self.prefs.play_sounds)
        self.append_cr_chk.SetValue(self.prefs.append_cr)
        self.username_txt.SetValue(self.prefs.netlinx_username)
        self.password_txt.SetValue(self.prefs.get_password())

        self.send_email_chk.SetValue(self.prefs.send_email_on_error)
        self.smtp_server_txt.SetValue(self.prefs.smtp_server)
        self.smtp_port_txt.SetValue(str(self.prefs.smtp_port))
        self.sender_email_txt.SetValue(self.prefs.sender_email)
        self.receiver_email_txt.SetValue(self.prefs.receiver_email)
        self.smtp_password_txt.SetValue(self.prefs.get_smtp_password())

    def on_save(self, event):
        self.prefs.camera = int(self.camera_txt.GetValue())
        if self.prefs.philips_testing != self.philips_chk.GetValue():
            self.prefs.philips_testing = self.philips_chk.GetValue()
            # We need to remove any steps and add a new philips step
            self.parent.action_list = []
            self.parent.action_list.append(self.parent.on_build_action(None))
            self.parent.refresh_actions()


        self.prefs.philips_testing = self.philips_chk.GetValue()
        self.prefs.play_sounds = self.play_sounds_chk.GetValue()
        self.prefs.append_cr = self.append_cr_chk.GetValue()
        self.prefs.netlinx_username = self.username_txt.GetValue()
        self.prefs.set_password(self.password_txt.GetValue())

        self.prefs.send_email_on_error = self.send_email_chk.GetValue()
        self.prefs.smtp_server = self.smtp_server_txt.GetValue()
        self.prefs.smtp_port = int(self.smtp_port_txt.GetValue())
        self.prefs.sender_email = self.sender_email_txt.GetValue()
        self.prefs.receiver_email = self.receiver_email_txt.GetValue()
        self.prefs.set_smtp_password(self.smtp_password_txt.GetValue())

        self.parent.save_config()
        self.Destroy()

    def on_cancel(self, event):
        self.Destroy()


