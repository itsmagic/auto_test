from pydispatch import dispatcher
from threading import Thread
import socket


class SVSiJobs(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.shutdown = False
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            action = self.queue.get()
            self.send_api_command(action)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def send_api_command(self, action):
        """Sends an api command to SVSi units"""
        port = 50001
        if action.append_cr:
            message = f"{action.command.command}\r"
        else:
            message = f"{action.command.command}"
        for ip_address in action.command.ip_addresses:
            sock = socket.socket(socket.AF_INET,     # Internet
                             socket.SOCK_STREAM)  # TCP
            sock.settimeout(2)
            sock.connect((ip_address, port))
            sock.send(message.encode())
            sock.close()
            # print('sending')
            dispatcher.send(signal="Status Update", message=f"Sent {ip_address} api command {action.command.command}")


