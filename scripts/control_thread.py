from threading import Thread
from pydispatch import dispatcher
from queue import Queue
from scripts.svsi_control import SVSiJobs
from scripts.camera_control import CameraJobs
from scripts.netlinx_control import NetlinxJobs
from scripts.dgx_shell_control import DGXShellJobs
from scripts.audio_control import AudioJobs
from scripts.philips_control import PhilipsJobs
import datetime
import time


class ControlThread(Thread):
    def __init__(self, prefs):
        self.print_log('Starting up')
        self.shutdown = False
        self.abort = False
        self.prefs = prefs
        self.action_queue = Queue()
        self.camera_queue = Queue()
        self.camera_thread = None
        self.svsi_queue = Queue()
        self.audio_queue = Queue()
        self.netlinx_queue = Queue()
        self.dgx_shell_queue = Queue()
        self.philips_queue = Queue()

        dispatcher.connect(self.status,
                           signal="Status Update",
                           sender=dispatcher.Any)
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)
        dispatcher.connect(self.print_log,
                           signal="Print Log")
        dispatcher.connect(self.abort_signal,
                           signal="Abort")
        Thread.__init__(self)

    def status(self, message):
        self.print_log(message)

    def shutdown_signal(self):
        """Shutsdown the thread"""
        self.shutdown = True

    def abort_signal(self, value=True):
        """Aborts any waiting processes"""
        self.abort = value

    def restart_camera(self):
        if self.camera_thread is not None:
            self.camera_thread.abort = True
            self.camera_thread.shutdown = True
            self.camera_thread.cam.release()
        self.camera_thread = CameraJobs(queue=self.camera_queue, prefs=self.prefs)
        self.camera_thread.daemon = True
        self.camera_thread.start()

    def run(self):
        self.print_log('Starting camera')
        self.restart_camera()
        self.print_log('Camera started')
        self.svsi_thread = SVSiJobs(self.svsi_queue)
        self.svsi_thread.daemon = True
        self.svsi_thread.start()
        self.netlinx_thread = NetlinxJobs(queue=self.netlinx_queue, prefs=self.prefs)
        self.netlinx_thread.daemon = True
        self.netlinx_thread.start()
        self.dgx_shell_thread = DGXShellJobs(queue=self.dgx_shell_queue, prefs=self.prefs)
        self.dgx_shell_thread.daemon = True
        self.dgx_shell_thread.start()
        self.audio_thread = AudioJobs(queue=self.audio_queue)
        self.audio_thread.daemon = True
        self.audio_thread.start()
        self.philips_thread = PhilipsJobs(self.philips_queue)
        self.philips_thread.daemon = True
        self.philips_thread.start()

        while not self.shutdown:
            # gets the job from the queue
            action = self.action_queue.get()
            if self.abort:
                try:
                    self.action_queue.task_done()
                    continue
                except Exception as error:
                    print('clearing queue error: ', error)
            self.process_action(action)

            # send a signal to the queue that the job is done
            self.action_queue.task_done()

    def process_action(self, action):
        # Check if it is valid
        if not action.valid:
            dispatcher.send(signal="Action Status", action=action, highlight=False)
            return
        # Check if it is a single_run
        if not action.single_run:
            # If not single run, check if it is enabled
            if not action.enabled:
                dispatcher.send(signal="Action Status", action=action, highlight=False)
                return

        dispatcher.send(signal="Action Status", action=action, highlight=True)

        if self.prefs.philips_testing:
            self.print_log(f"Sending command to Philips Display at ip address: {action.command.ip_addresses[0]}")
            self.philips_queue.put(action)
            self.philips_queue.join()
            dispatcher.send(signal="Action Status", action=action, highlight=False)
            return

        if action.command.system.lower() == 'wait':
            self.print_log(f'Waiting for {action.command.command} seconds')
            for i in range(action.command.command):
                dispatcher.send(signal="Timer", data=f"{action.command.command - i}")
                if self.abort:
                    break
                time.sleep(1)
            dispatcher.send(signal="Timer", data="")

        if action.command.system.lower() == 'svsi':
            self.print_log(f'Sending command to SVSi ip addresses: {action.command.ip_addresses} and executing api command {action.command.command}')
            action.append_cr = self.prefs.append_cr
            self.svsi_queue.put(action)
            self.svsi_queue.join()

        if action.command.system.lower() == 'camera':
            if action.command.command != "show_camera":
                self.print_log(f'Camera scanning for {action.command.wait_time} seconds, expecting qr code(s) to contain {action.command.expected_text_list}')
            # else:
            #     self.print_log(f'In show camera command')
            self.camera_queue.put(action)
            self.camera_queue.join()

        if action.command.system.lower() == 'check_camera':
            self.print_log(f'Checking camera')
            self.camera_queue.put(action)
            self.camera_queue.join()

        if action.command.system.lower() == 'netlinx':
            self.print_log(f"Sending Netlinx command: {action.command.command.strip()} to {action.command.ip_addresses}")
            self.netlinx_queue.put(action)
            self.netlinx_queue.join()

        if action.command.system.lower() == 'dgx':
            self.print_log(f"Sending DGX Shell command: {action.command.command.strip()} to {action.command.ip_addresses}")
            self.dgx_shell_queue.put(action)
            self.dgx_shell_queue.join()

        if action.command.system.lower() == 'sound':
            self.print_log(f"Playing sound")
            self.audio_queue.put(action)
            self.audio_queue.join()

        dispatcher.send(signal="Action Status", action=action, highlight=False)

    def send_status(self, status):
        dispatcher.send(signal="Comms Status", sender=status)

    def print_log(self, log):
        now = str(datetime.datetime.now().isoformat())
        my_log = f'{now} {log}'
        print(my_log)
        dispatcher.send(signal="Log", message=my_log)


def main():
    pass


if __name__ == '__main__':
    main()
