from winsound import PlaySound
from pydispatch import dispatcher
from threading import Thread
import wx

class AudioJobs(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.shutdown = False
        print("audio jobs started")
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            action = self.queue.get()
            self.play_sound(action)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def shutdown_signal(self, sender):
        """Shutsdown the thread"""
        self.shutdown = True

    def play_sound(self, action):
        """Plays a sound on the computer"""
        filename = "sounds\\woof.wav"
        sound = wx.adv.Sound(action.command.command)
        if sound.IsOk():
            sound.Play(wx.adv.SOUND_SYNC)
        else:
            dispatcher.send(signal="Status Update", message=f"Invalid or unable to find file {action.command.command}")
        dispatcher.send(signal="Status Update", message=f"Played sound file {action.command.command}")


