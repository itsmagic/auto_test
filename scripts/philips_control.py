from pydispatch import dispatcher
from threading import Thread
import socket
import os
import json
import ipaddress

class PhilipsJobs(Thread):

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.shutdown = False
        dispatcher.connect(self.shutdown_signal,
                           signal="Shutdown",
                           sender=dispatcher.Any)

    def run(self):
        while not self.shutdown:
            # gets the job from the queue
            action = self.queue.get()
            self.send_api_command(action)

            # send a signal to the queue that the job is done
            self.queue.task_done()

    def shutdown_signal(self, sender):
        """Shuts down the thread"""
        self.shutdown = True

    def send_api_command(self, action):
        """Sends an api command to Philips unit"""
        port = 5000
        timeout = 5
        message = b''.join([ bytes.fromhex(piece) for piece in action.command.command.split()])


        for ip_address in action.command.ip_addresses:
            try:
                my_ip = ipaddress.ip_address(ip_address)
            except ValueError as error:
                dispatcher.send(signal="Status Update", message=f"Invalid IP {ip_address} :: {error}")
                return

            sock = socket.socket(socket.AF_INET,     # Internet
                             socket.SOCK_STREAM)     # TCP
            sock.settimeout(2)
            try:
                sock.connect((ip_address, port))
            except TimeoutError:
                dispatcher.send(signal="Status Update", message=f"Unable to connect to {ip_address}")
                return
            sock.send(message)
            dispatcher.send(signal="Status Update", message=f"Sent {ip_address} {action.name} {action.command.command}")
            sock.settimeout(timeout)
            try:
                response = self.decode_to_readable(sock.recv(1024))
                # Check response 
                if not os.path.exists("response_lookup.json"):
                    with open("response_lookup.json", "w") as f:
                        json.dump({}, f)
                with open("response_lookup.json", "r") as f:
                    response_lookup = json.load(f)
                if response in response_lookup:
                    dispatcher.send(signal="Status Update", message=f"Response to {action.name} {action.command.command} :: {response} :: {response_lookup[response]}")
                
                elif "07 01 01 C1 00 " in response:
                    try:
                        channel = f"Channel: {int(response.split()[5], 16)}"
                    except Exception as error:
                        print(f"Unable to get channel number: {error}")
                        channel = "Unable to get channel number"
                    dispatcher.send(signal="Status Update", message=f"Response to {action.name} {action.command.command} :: {response} :: {channel}")
                    

                else:
                    dispatcher.send(signal="Status Update", message=f"Response to {action.name} {action.command.command} :: {response}")
            except TimeoutError:
                dispatcher.send(signal="Status Update", message=f"No response received to {action.name} {action.command.command} before timeout {timeout} seconds")
            sock.close()

    def decode_to_readable(self, message):
        readable = " ".join([ hex(piece)[2:].zfill(2).upper() for piece in bytearray(message)])
        return readable

