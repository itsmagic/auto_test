import os
import pyqrcode
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


if not os.path.exists(os.path.join('qr_codes')):
    os.makedirs(os.path.join('qr_codes'))
for i in range(10):
    qr = pyqrcode.create("Test " + str(i + 1))
    qr.png(os.path.join('qr_codes', "Test_qr" + str(i + 1) + '.png'), scale=5)
font = ImageFont.load_default().font
font = ImageFont.truetype("Verdana.ttf", 14)
for i in range(10):
    img = Image.open(os.path.join('qr_codes', "Test_" + str(i + 1) + '.png'))
    draw = ImageDraw.Draw(img)
    draw.text(xy=(50, 128), text="Test_" + str(i + 1), fill="black", font=font)
    img.save(os.path.join('qr_codes', "Test_" + str(i + 1) + '.png'))
