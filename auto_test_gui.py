# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.adv

###########################################################################
## Class AutoTestFrame
###########################################################################

class AutoTestFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Auto Test", pos = wx.DefaultPosition, size = wx.Size( -1,-1 ), style = wx.DEFAULT_FRAME_STYLE|wx.RESIZE_BORDER|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 750,650 ), wx.Size( -1,-1 ) )

		bSizer1 = wx.BoxSizer( wx.VERTICAL )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		bSizer7 = wx.BoxSizer( wx.VERTICAL )

		bSizer7.SetMinSize( wx.Size( -1,200 ) )
		self.m_scrolledWindow1 = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_scrolledWindow1.SetScrollRate( 5, 5 )
		self.step_sizer = wx.BoxSizer( wx.VERTICAL )


		self.m_scrolledWindow1.SetSizer( self.step_sizer )
		self.m_scrolledWindow1.Layout()
		self.step_sizer.Fit( self.m_scrolledWindow1 )
		bSizer7.Add( self.m_scrolledWindow1, 1, wx.EXPAND, 5 )


		bSizer3.Add( bSizer7, 1, wx.EXPAND, 5 )

		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel2 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer31 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticline41 = wx.StaticLine( self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer31.Add( self.m_staticline41, 0, wx.EXPAND |wx.ALL, 5 )

		bSizer27 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer16 = wx.BoxSizer( wx.VERTICAL )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel2, wx.ID_ANY, u"Controls" ), wx.HORIZONTAL )

		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )

		self.enable_all_btn = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Enable All", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer10.Add( self.enable_all_btn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.run_commands_btn = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Run Commands", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.run_commands_btn.SetMinSize( wx.Size( 110,-1 ) )

		bSizer10.Add( self.run_commands_btn, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.m_staticText1 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"X", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText1.Wrap( -1 )

		bSizer10.Add( self.m_staticText1, 0, wx.TOP|wx.BOTTOM|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.num_loops_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, u"1", wx.DefaultPosition, wx.Size( 60,-1 ), 0 )
		bSizer10.Add( self.num_loops_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.TOP|wx.BOTTOM|wx.LEFT, 5 )

		bSizer18 = wx.BoxSizer( wx.VERTICAL )

		self.repeat_chk = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Repeat", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer18.Add( self.repeat_chk, 0, wx.ALL, 5 )

		self.stop_on_error_chk = wx.CheckBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Stop On Error", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer18.Add( self.stop_on_error_chk, 0, wx.TOP|wx.RIGHT|wx.LEFT, 5 )


		bSizer10.Add( bSizer18, 0, wx.ALL|wx.EXPAND, 5 )


		sbSizer2.Add( bSizer10, 1, wx.EXPAND, 5 )

		self.m_staticline4 = wx.StaticLine( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		sbSizer2.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )

		bSizer28 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText8 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"Timer", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )

		bSizer28.Add( self.m_staticText8, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.count_down_timer_txt = wx.TextCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer28.Add( self.count_down_timer_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer2.Add( bSizer28, 1, wx.EXPAND, 5 )


		bSizer16.Add( sbSizer2, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer1 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel2, wx.ID_ANY, u"Results" ), wx.VERTICAL )

		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )

		bSizer14 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText5 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Current Loop:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )

		bSizer14.Add( self.m_staticText5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.current_loop_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer14.Add( self.current_loop_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer15.Add( bSizer14, 0, wx.EXPAND, 5 )

		bSizer12 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText2 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Successful QR reads: ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText2.Wrap( -1 )

		bSizer12.Add( self.m_staticText2, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.success_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer12.Add( self.success_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer15.Add( bSizer12, 0, wx.EXPAND, 5 )

		bSizer13 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText3 = wx.StaticText( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Failed QR reads:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )

		bSizer13.Add( self.m_staticText3, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.failed_txt = wx.TextCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer13.Add( self.failed_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer15.Add( bSizer13, 0, wx.EXPAND, 5 )

		bSizer17 = wx.BoxSizer( wx.VERTICAL )

		self.qr_codes_link_txt = wx.adv.HyperlinkCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Sample QR codes", u"file:\\qr_codes", wx.DefaultPosition, wx.DefaultSize, wx.adv.HL_DEFAULT_STYLE )
		bSizer17.Add( self.qr_codes_link_txt, 0, wx.ALL, 5 )

		self.failed_imgs_link = wx.adv.HyperlinkCtrl( sbSizer1.GetStaticBox(), wx.ID_ANY, u"Images Folder", u"file:\\", wx.DefaultPosition, wx.DefaultSize, wx.adv.HL_DEFAULT_STYLE )
		bSizer17.Add( self.failed_imgs_link, 0, wx.ALL, 5 )


		bSizer15.Add( bSizer17, 1, wx.EXPAND, 5 )


		sbSizer1.Add( bSizer15, 1, wx.EXPAND, 5 )


		bSizer16.Add( sbSizer1, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticline3 = wx.StaticLine( self.m_panel2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer16.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )

		bSizer8 = wx.BoxSizer( wx.VERTICAL )

		bSizer11 = wx.BoxSizer( wx.VERTICAL )

		self.debug_txt = wx.TextCtrl( self.m_panel2, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
		self.debug_txt.SetMinSize( wx.Size( 250,200 ) )

		bSizer11.Add( self.debug_txt, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer8.Add( bSizer11, 1, wx.EXPAND, 5 )

		bSizer121 = wx.BoxSizer( wx.HORIZONTAL )

		self.debug_clear_btn = wx.Button( self.m_panel2, wx.ID_ANY, u"Clear", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer121.Add( self.debug_clear_btn, 0, wx.ALL, 5 )


		bSizer8.Add( bSizer121, 0, wx.ALIGN_RIGHT, 5 )


		bSizer16.Add( bSizer8, 0, wx.EXPAND, 5 )


		bSizer27.Add( bSizer16, 1, wx.EXPAND, 5 )

		bSizer26 = wx.BoxSizer( wx.VERTICAL )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel2, wx.ID_ANY, u"Camera" ), wx.VERTICAL )

		bSizer29 = wx.BoxSizer( wx.VERTICAL )

		self.cam_view_bmp = wx.StaticBitmap( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.cam_view_bmp.SetMinSize( wx.Size( 320,240 ) )

		bSizer29.Add( self.cam_view_bmp, 0, wx.ALL, 5 )


		sbSizer4.Add( bSizer29, 1, wx.EXPAND, 5 )

		bSizer30 = wx.BoxSizer( wx.HORIZONTAL )

		self.checking_cancel_btn = wx.Button( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Show Camera Preview", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer30.Add( self.checking_cancel_btn, 0, wx.ALL, 5 )


		sbSizer4.Add( bSizer30, 0, wx.ALIGN_RIGHT, 5 )


		bSizer26.Add( sbSizer4, 1, wx.EXPAND|wx.ALL, 5 )


		bSizer27.Add( bSizer26, 0, 0, 5 )


		bSizer31.Add( bSizer27, 1, wx.EXPAND, 5 )


		self.m_panel2.SetSizer( bSizer31 )
		self.m_panel2.Layout()
		bSizer31.Fit( self.m_panel2 )
		bSizer6.Add( self.m_panel2, 1, wx.EXPAND, 5 )


		bSizer3.Add( bSizer6, 0, wx.EXPAND, 5 )


		bSizer1.Add( bSizer3, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer1 )
		self.Layout()
		bSizer1.Fit( self )
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menubar1.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		self.file_menu = wx.Menu()
		self.save_menu = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Save", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.save_menu )

		self.load_menu = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Load", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.load_menu )

		self.m_menuItem7 = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Preferences", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.m_menuItem7 )

		self.m_menuItem6 = wx.MenuItem( self.file_menu, wx.ID_ANY, u"Quit", wx.EmptyString, wx.ITEM_NORMAL )
		self.file_menu.Append( self.m_menuItem6 )

		self.m_menubar1.Append( self.file_menu, u"File" )

		self.help_menu = wx.Menu()
		self.action_help_menu = wx.MenuItem( self.help_menu, wx.ID_ANY, u"Command Help", wx.EmptyString, wx.ITEM_NORMAL )
		self.help_menu.Append( self.action_help_menu )

		self.m_menubar1.Append( self.help_menu, u"Help" )

		self.SetMenuBar( self.m_menubar1 )


		self.Centre( wx.BOTH )

		# Connect Events
		self.enable_all_btn.Bind( wx.EVT_BUTTON, self.on_enable_all_toggle )
		self.run_commands_btn.Bind( wx.EVT_BUTTON, self.on_run_actions )
		self.num_loops_txt.Bind( wx.EVT_TEXT, self.on_config )
		self.repeat_chk.Bind( wx.EVT_CHECKBOX, self.on_config )
		self.stop_on_error_chk.Bind( wx.EVT_CHECKBOX, self.on_config )
		self.qr_codes_link_txt.Bind( wx.adv.EVT_HYPERLINK, self.on_sample_qrs )
		self.failed_imgs_link.Bind( wx.adv.EVT_HYPERLINK, self.on_images_folder )
		self.debug_clear_btn.Bind( wx.EVT_BUTTON, self.on_clear_debug )
		self.checking_cancel_btn.Bind( wx.EVT_BUTTON, self.on_cancel_checking )
		self.Bind( wx.EVT_MENU, self.on_save, id = self.save_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_load, id = self.load_menu.GetId() )
		self.Bind( wx.EVT_MENU, self.on_preferences, id = self.m_menuItem7.GetId() )
		self.Bind( wx.EVT_MENU, self.on_action_help, id = self.action_help_menu.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_enable_all_toggle( self, event ):
		event.Skip()

	def on_run_actions( self, event ):
		event.Skip()

	def on_config( self, event ):
		event.Skip()



	def on_sample_qrs( self, event ):
		event.Skip()

	def on_images_folder( self, event ):
		event.Skip()

	def on_clear_debug( self, event ):
		event.Skip()

	def on_cancel_checking( self, event ):
		event.Skip()

	def on_save( self, event ):
		event.Skip()

	def on_load( self, event ):
		event.Skip()

	def on_preferences( self, event ):
		event.Skip()

	def on_action_help( self, event ):
		event.Skip()


###########################################################################
## Class HelpWindow
###########################################################################

class HelpWindow ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Help", pos = wx.DefaultPosition, size = wx.Size( 800,495 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer9 = wx.BoxSizer( wx.VERTICAL )

		self.m_textCtrl3 = wx.TextCtrl( self, wx.ID_ANY, u"    The idea here is to have a text formatted command that can be translated to actions.\n    Format is comma separated\n    First field is type of system (not case sensitive)\n    Netlinx, DGX, SVSi, Camera, or Sound\n\n    For Netlinx\n    Netlinx|<ip_address>|<on or off>|<d:p:s,c>\n    Example\n    netlinx|192.168.1.2|on|5002:1:2,1\n    netlinx|192.168.1.2|send_command 10001:1:0,\"'setup'\"|\n\n    For DGX Shell\n         - Leave the last command blank for normal commands\n         - Set the last command to True to elevate privileges prior to sending command\n    DGX|<ip_address>|<command>|<elevate privileges>\n    Example\n    dgx|198.18.128.1|set mcpu_prequal_disabled OFF|\n    dgx|198.18.128.1|power OFF o1|True\n\n    For SVSi\n    SVSi|<ip_address_list>|<api_command>\n    Example\n    svsi|192.168.1.2,192.168.1.3|seta 102\n\n    For Camera\n        - wait time to 0 to not wait\n        - expected result is always a list ie Code 1,2,3\n    Camera|QR|<wait time>|<expected result>\n    Example\n    camera|QR|10|['Code 1']\n\n    For Wait\n    Wait|<seconds to wait>\n    Example\n    wait|10\n\n    For Sound\n        - file should be stored in the same directory as program\n    Sound|<filename>\n    Example\n    sound|woof.wav", wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
		bSizer9.Add( self.m_textCtrl3, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer9 )
		self.Layout()

		self.Centre( wx.BOTH )

	def __del__( self ):
		pass


###########################################################################
## Class PreferencesMenu
###########################################################################

class PreferencesMenu ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Preferences", pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer18 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel3 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer19 = wx.BoxSizer( wx.VERTICAL )

		bSizer20 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText5 = wx.StaticText( self.m_panel3, wx.ID_ANY, u"Camera", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )

		bSizer20.Add( self.m_staticText5, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.camera_txt = wx.TextCtrl( self.m_panel3, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer20.Add( self.camera_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		bSizer19.Add( bSizer20, 0, wx.EXPAND|wx.ALL, 5 )

		bSizer21 = wx.BoxSizer( wx.VERTICAL )

		self.philips_chk = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Test Philips SICP", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.philips_chk, 0, wx.ALL, 5 )

		self.play_sounds_chk = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Play Sounds", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.play_sounds_chk.Enable( False )

		bSizer21.Add( self.play_sounds_chk, 0, wx.ALL, 5 )

		self.append_cr_chk = wx.CheckBox( self.m_panel3, wx.ID_ANY, u"Append carriage return to SVSi Commands", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.append_cr_chk, 0, wx.ALL, 5 )


		bSizer19.Add( bSizer21, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel3, wx.ID_ANY, u"Netlinx" ), wx.VERTICAL )

		bSizer24 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText6 = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )

		self.m_staticText6.SetMinSize( wx.Size( 60,-1 ) )

		bSizer24.Add( self.m_staticText6, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.username_txt = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.username_txt.SetMinSize( wx.Size( 120,-1 ) )

		bSizer24.Add( self.username_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer4.Add( bSizer24, 0, wx.EXPAND, 5 )

		bSizer25 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText7 = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )

		self.m_staticText7.SetMinSize( wx.Size( 60,-1 ) )

		bSizer25.Add( self.m_staticText7, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.password_txt = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		self.password_txt.SetMinSize( wx.Size( 120,-1 ) )

		bSizer25.Add( self.password_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer4.Add( bSizer25, 0, wx.EXPAND, 5 )


		bSizer19.Add( sbSizer4, 0, wx.EXPAND|wx.ALL, 5 )

		sbSizer5 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel3, wx.ID_ANY, u"Email" ), wx.VERTICAL )

		self.send_email_chk = wx.CheckBox( sbSizer5.GetStaticBox(), wx.ID_ANY, u"Send Email on Error", wx.DefaultPosition, wx.DefaultSize, 0 )
		sbSizer5.Add( self.send_email_chk, 0, wx.ALL, 5 )

		bSizer31 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText9 = wx.StaticText( sbSizer5.GetStaticBox(), wx.ID_ANY, u"SMTP Server", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText9.Wrap( -1 )

		self.m_staticText9.SetMinSize( wx.Size( 100,-1 ) )

		bSizer31.Add( self.m_staticText9, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.smtp_server_txt = wx.TextCtrl( sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.smtp_server_txt.SetMinSize( wx.Size( 220,-1 ) )

		bSizer31.Add( self.smtp_server_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer5.Add( bSizer31, 0, wx.EXPAND, 5 )

		bSizer311 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText91 = wx.StaticText( sbSizer5.GetStaticBox(), wx.ID_ANY, u"SMTP Port", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText91.Wrap( -1 )

		self.m_staticText91.SetMinSize( wx.Size( 100,-1 ) )

		bSizer311.Add( self.m_staticText91, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.smtp_port_txt = wx.TextCtrl( sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.smtp_port_txt.SetMinSize( wx.Size( 220,-1 ) )

		bSizer311.Add( self.smtp_port_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer5.Add( bSizer311, 1, wx.EXPAND, 5 )

		bSizer312 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText92 = wx.StaticText( sbSizer5.GetStaticBox(), wx.ID_ANY, u"Sender Email", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText92.Wrap( -1 )

		self.m_staticText92.SetMinSize( wx.Size( 100,-1 ) )

		bSizer312.Add( self.m_staticText92, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.sender_email_txt = wx.TextCtrl( sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.sender_email_txt.SetMinSize( wx.Size( 220,-1 ) )

		bSizer312.Add( self.sender_email_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer5.Add( bSizer312, 1, wx.EXPAND, 5 )

		bSizer313 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText93 = wx.StaticText( sbSizer5.GetStaticBox(), wx.ID_ANY, u"Receiver Email", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText93.Wrap( -1 )

		self.m_staticText93.SetMinSize( wx.Size( 100,-1 ) )

		bSizer313.Add( self.m_staticText93, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.receiver_email_txt = wx.TextCtrl( sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.receiver_email_txt.SetMinSize( wx.Size( 220,-1 ) )

		bSizer313.Add( self.receiver_email_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer5.Add( bSizer313, 1, wx.EXPAND, 5 )

		bSizer314 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText94 = wx.StaticText( sbSizer5.GetStaticBox(), wx.ID_ANY, u"SMTP Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText94.Wrap( -1 )

		self.m_staticText94.SetMinSize( wx.Size( 100,-1 ) )

		bSizer314.Add( self.m_staticText94, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )

		self.smtp_password_txt = wx.TextCtrl( sbSizer5.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		self.smtp_password_txt.SetMinSize( wx.Size( 220,-1 ) )

		bSizer314.Add( self.smtp_password_txt, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )


		sbSizer5.Add( bSizer314, 1, wx.EXPAND, 5 )


		bSizer19.Add( sbSizer5, 0, wx.EXPAND|wx.ALL, 5 )

		bSizer26 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button5 = wx.Button( self.m_panel3, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer26.Add( self.m_button5, 0, wx.ALL, 5 )

		self.m_button6 = wx.Button( self.m_panel3, wx.ID_ANY, u"Cancel", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer26.Add( self.m_button6, 0, wx.ALL, 5 )


		bSizer19.Add( bSizer26, 0, wx.ALIGN_RIGHT, 5 )


		self.m_panel3.SetSizer( bSizer19 )
		self.m_panel3.Layout()
		bSizer19.Fit( self.m_panel3 )
		bSizer18.Add( self.m_panel3, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer18 )
		self.Layout()
		bSizer18.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button5.Bind( wx.EVT_BUTTON, self.on_save )
		self.m_button6.Bind( wx.EVT_BUTTON, self.on_cancel )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def on_save( self, event ):
		event.Skip()

	def on_cancel( self, event ):
		event.Skip()


