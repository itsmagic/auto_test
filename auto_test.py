import wx
import os
import sys
import datetime
import cv2
import auto_test_gui
import webbrowser
import json
import pickle
import smtplib
import ssl
from license_base import LicenseBase
from email.mime.text import MIMEText
from random import random
from pydispatch import dispatcher
from threading import Thread
import scripts.control_thread as control_thread
from scripts import auto_update, preferences_menu
from scripts.datastore import Action, Preferences

# The MIT License (MIT)

# Copyright (c) 2021 Jim Maciejewski

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


class Auto_Test_Frame(auto_test_gui.AutoTestFrame):
    def __init__(self, parent):
        auto_test_gui.AutoTestFrame.__init__(self, parent)

        icon_bundle = wx.IconBundle()
        icon_bundle.AddIcon(self.resource_path(os.path.join("icon", "at.ico")), wx.BITMAP_TYPE_ANY)
        self.SetIcons(icon_bundle)
        self.name = "Auto Test"
        self.version = "v0.1.9"
        self.path = os.path.expanduser(os.path.join('~', 'Documents', self.name))
        self.images_path = os.path.join(self.path, 'images')
        if not os.path.exists(self.images_path):
            os.makedirs(os.path.join(self.images_path, 'failed'))
            os.makedirs(os.path.join(self.images_path, 'success'))
        self.SetTitle(f'{self.name} {self.version}')

        self.action_list = []
        self.initial_conf = True
        self.prefs = self.load_config()
        

        if self.action_list == []:
            # Add sample action
            self.action_list.append(self.on_build_action(None))

        for action in self.action_list:
            action.update_text(philips=self.prefs.philips_testing)

        self.abort = False
        self.success = 0
        self.failed = 0
        self.refresh_actions()

        self.num_loops_txt.SetValue(self.prefs.num_loops_txt)
        self.repeat_chk.SetValue(self.prefs.repeat_chk)
        self.stop_on_error_chk.SetValue(self.prefs.stop_on_error_chk)
        self.initial_conf = False
        self.qr_codes_link_txt.SetURL(self.resource_path(os.path.join('qrcodes')))
        self.failed_imgs_link.SetURL(os.path.join(self.path, 'images'))

        dispatcher.connect(self.debug_log, signal="Log")
        dispatcher.connect(self.action_status, signal="Action Status")
        dispatcher.connect(self.save_photo, signal="Save Photo")
        dispatcher.connect(self.update_timer, signal="Timer")
        dispatcher.connect(self.update_preview, signal="Preview")

        self.control_thread = control_thread.ControlThread(self.prefs)
        self.control_thread.daemon = True
        self.control_thread.start()

        self.first_frame = True

        dispatcher.connect(self.update_required,
                           signal="Software Update",
                           sender=dispatcher.Any)
        self.check_for_updates = True
        if self.check_for_updates:
            update_thread = auto_update.AutoUpdate(server_url="https://magicsoftware.ornear.com", program_name=self.name, program_version=self.version)
            update_thread.daemon = True
            update_thread.start()

        self.license_thread = LicenseBase(name=self.name, version=self.version)
        self.license_thread.daemon = True
        self.license_thread.start()

    def update_preview(self, frame, width, height):

        new_frame = frame[:]
        new_frame = cv2.resize(new_frame, (320, 240))
        rgb = cv2.cvtColor(new_frame, cv2.COLOR_BGR2RGB)
        image = wx.Bitmap.FromBuffer(320, 240, rgb)

        self.cam_view_bmp.SetBitmap(image)

    def on_check_webcam(self, event=None):
        """Check the webcam"""
        my_action = Action(text="camera|show_camera")
        my_action.update_text()
        dispatcher.send(signal="Abort", value=False)
        dispatcher.send(signal="Checking", value=True)
        self.control_thread.action_queue.put(my_action)

    def on_cancel_checking(self, event=None):
        if self.checking_cancel_btn.GetLabel() == "Show Camera Preview":
            if self.run_commands_btn.GetLabel() == "Cancel":
                return
            self.checking_cancel_btn.SetLabel("Stop Preview")
            # self.Fit()
            # self.Layout()
            self.on_check_webcam()
        else:
            self.checking_cancel_btn.SetLabel("Show Camera Preview")
            # self.Fit()
            # self.Layout()
            dispatcher.send(signal="Checking", value=False)

        # self.Fit()
        # self.Layout()

    def on_preferences(self, event):
        """"""
        dia = preferences_menu.PreferencesConfig(self)
        dia.ShowModal()
        dia.Destroy()
        self.control_thread.restart_camera()
        self.checking_cancel_btn.SetLabel("Show Camera Preview")

    def on_config(self, event=None):
        if self.initial_conf:
            return
        # Get current config items and save them
        self.prefs.num_loops_txt = self.num_loops_txt.GetValue()
        self.prefs.repeat_chk = self.repeat_chk.GetValue()
        self.prefs.stop_on_error_chk = self.stop_on_error_chk.GetValue()
        self.save_config()

    def update_timer(self, data):
        self.count_down_timer_txt.SetValue(f"{data}")

    def action_status(self, action, highlight):
        # print(action)
        if action.panel is None:
            return
        if highlight:
            action.panel.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_HIGHLIGHT))
        else:
            action.panel.SetBackgroundColour(wx.NullColour)
            # Since we are done with this action -- we need to check if we are at the end of the list...

            if action.single_run:
                action.single_run = False
                self.run_commands_btn.SetLabel('Run Commands')
                self.Refresh()
                return

            if self.action_list[-1] == action:
                # last one is done
                self.prefs.num_loops_txt = str(int(self.prefs.num_loops_txt) + 1)
                # Check if loops are complete
                if int(self.prefs.num_loops_txt) <= int(self.num_loops_txt.GetValue()) or self.repeat_chk.GetValue():
                    self.queue_actions()
                    self.current_loop_txt.SetValue(str(self.prefs.num_loops_txt))
                else:
                    self.run_commands_btn.SetLabel('Run Commands')
                    self.checking_cancel_btn.Enable()
        self.Refresh()

    def save_photo(self, frame, successful, now, action):
        # print('in save photo')
        if successful:
            self.success += 1
            self.success_txt.SetValue(str(self.success))
            cv2.imwrite(os.path.join(self.images_path, 'success', f'{now}_loop-{self.current_loop_txt.GetValue()}.jpeg'), frame)
        else:
            self.failed += 1
            self.failed_txt.SetValue(str(self.failed))
            cv2.imwrite(os.path.join(self.images_path, 'failed', f'{now}_loop-{self.current_loop_txt.GetValue()}.jpeg'), frame)
            if self.stop_on_error_chk.GetValue():
                self.on_run_actions()
                dispatcher.send(signal="Print Log", log="Stopped On Error")
                if self.prefs.send_email_on_error and not action.single_run:
                    Thread(self.send_email()).start()

    def send_email(self):
        try:
            for recepient in self.prefs.receiver_email.split(','):
                msg = MIMEText(f'Hi Auto Test User,\r\rAuto test has stopped on an error.\rGood luck with your troubleshooting!')
                msg['Subject'] = f"Auto test has stopped {datetime.datetime.now().isoformat()}"
                msg['From'] = self.prefs.sender_email
                msg['To'] = recepient

                # message = """\
                # Subject: Hi there

                # Auto test has stopped on error."""

                context = ssl.create_default_context()
                with smtplib.SMTP_SSL(self.prefs.smtp_server, self.prefs.smtp_port, context=context) as server:
                    server.login(self.prefs.sender_email, self.prefs.get_smtp_password())
                    server.sendmail(self.prefs.sender_email, recepient, msg.as_string())
                dispatcher.send(signal="Print Log", log=f"Sending Notification Email to {recepient}")
        except Exception as error:
            dispatcher.send(signal="Print Log", log=f"Unable to send email {error}")

    def debug_log(self, message):
        """Updates progress in main"""
        # print status
        self.debug_txt.AppendText(f'{message}\r')
        with open(os.path.join(self.path, 'audit.log'), 'a') as f:
            f.write(f'{message}\n')

    def on_enable_all_toggle(self, event):
        """Enable all commands"""
        if self.enable_all_btn.GetLabel() == 'Enable All':
            for item in self.action_list:
                item.enabled = True
            self.enable_all_btn.SetLabel('Disable All')
        else:
            for item in self.action_list:
                item.enabled = False
            self.enable_all_btn.SetLabel('Enable All')
        self.refresh_actions()

    def on_run_actions(self, event=None):
        """Toggles between running and cancel"""
        if self.run_commands_btn.GetLabel() == 'Run Commands':
            if self.checking_cancel_btn.GetLabel() == "Stop Preview":
                self.on_cancel_checking()
            self.checking_cancel_btn.Disable()
            self.run_commands_btn.SetLabel('Cancel')
            dispatcher.send(signal="Abort", value=False)
            self.prefs.num_loops_txt = "1"
            self.current_loop_txt.SetValue(self.prefs.num_loops_txt)
            self.success = 0
            self.success_txt.SetValue(str(self.success))
            self.failed = 0
            self.failed_txt.SetValue(str(self.failed))
            self.queue_actions()
        else:
            dispatcher.send(signal="Abort", value=True)
            self.run_commands_btn.SetLabel('Run Commands')
            self.checking_cancel_btn.Enable()

    def queue_actions(self):
        for action in self.action_list:
            self.control_thread.action_queue.put(action)

    def get_panels(self):
        """Gets all the panels from the window"""
        panels = []
        children = self.m_scrolledWindow1.GetChildren()
        for item in children:
            if isinstance(item, wx.Panel):
                panels.append(item)
        return panels

    def remove_all_actions(self):
        """Removes all actions"""
        # self.record_current_values()
        panels = self.get_panels()
        my_sizers = []
        for panel in panels:
            my_sizers.append(panel.GetChildren()[0])
        for sizer in my_sizers:
            sizer.Hide()
            sizer.DestroyChildren()
            sizer.GetParent().Destroy()

    def on_build_action(self, event=None, enabled=True, text='wait|10'):
        action = Action(enabled=enabled, text=text)
        return action

    def refresh_actions(self):

        self.remove_all_actions()

        for i, action in enumerate(self.action_list):
            action.update_text(philips=self.prefs.philips_testing)

            if not self.prefs.philips_testing:

                # Add panel
                new_panel = wx.Panel(self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)

                # Add sizer
                new_sizer = wx.StaticBoxSizer(wx.StaticBox(new_panel, wx.ID_ANY, u"Step " + str(i + 1)), wx.HORIZONTAL)

                # Add enable check
                enable_chk = wx.CheckBox(new_sizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
                new_sizer.Add(enable_chk, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)
                enable_chk.SetValue(bool(action.enabled))
                enable_chk.Bind(wx.EVT_CHECKBOX, self.on_update_chk)

                # Add command_txt
                action_txt = wx.TextCtrl(new_sizer.GetStaticBox(), wx.ID_ANY, action.text, wx.DefaultPosition, wx.DefaultSize, 0)
                new_sizer.Add(action_txt, 1, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)
                action_txt.SetMinSize(wx.Size(300, -1))
                action_txt.Bind(wx.EVT_TEXT, self.on_update_text)

                # Valid btn
                if action.valid:
                    valid_btn = wx.Button(new_sizer.GetStaticBox(), wx.ID_ANY, u"Test Command", wx.DefaultPosition, wx.DefaultSize, 0)
                    valid_btn.SetBackgroundColour('green')
                else:
                    valid_btn = wx.Button(new_sizer.GetStaticBox(), wx.ID_ANY, u"Invalid", wx.DefaultPosition, wx.DefaultSize, 0)
                    valid_btn.SetBackgroundColour('red')
                valid_btn.SetMinSize(wx.Size(100, -1))
                new_sizer.Add(valid_btn, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
                valid_btn.Bind(wx.EVT_BUTTON, self.on_test_action)

                # Add up btn
                up_btn = wx.BitmapButton(new_sizer.GetStaticBox(), wx.ID_ANY, wx.Bitmap(self.resource_path(os.path.join("images", "up.bmp")), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
                new_sizer.Add(up_btn, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
                up_btn.SetToolTip(u"Move this step up")
                up_btn.Bind(wx.EVT_BUTTON, self.on_up)

                # Add down btn
                down_btn = wx.BitmapButton(new_sizer.GetStaticBox(), wx.ID_ANY, wx.Bitmap(self.resource_path(os.path.join("images", "down.bmp")), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
                new_sizer.Add(down_btn, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
                down_btn.SetToolTip(u"Move this step down")
                down_btn.Bind(wx.EVT_BUTTON, self.on_down)

                # Add add btn
                add_btn = wx.BitmapButton(new_sizer.GetStaticBox(), wx.ID_ANY, wx.Bitmap(self.resource_path(os.path.join("images", "add.bmp")), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
                new_sizer.Add(add_btn, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
                add_btn.SetToolTip(u"Duplicate this step")
                add_btn.Bind(wx.EVT_BUTTON, self.on_add)

                # Add trash btn
                trash_btn = wx.BitmapButton(new_sizer.GetStaticBox(), wx.ID_ANY, wx.Bitmap(self.resource_path(os.path.join("images", "trash.bmp")), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
                new_sizer.Add(trash_btn, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
                trash_btn.SetToolTip(u"Remove this step")
                trash_btn.Bind(wx.EVT_BUTTON, self.on_delete)

                new_panel.SetSizer(new_sizer)
                new_panel.Layout()
                action.panel = new_panel
                new_sizer.Fit(new_panel)
                self.step_sizer.Add(new_panel, 0, wx.EXPAND | wx.ALL, 5)
            
            else:
                # Philips style
                new_panel = wx.Panel( self.m_scrolledWindow1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
                new_sizer = wx.StaticBoxSizer( wx.StaticBox( new_panel, wx.ID_ANY, u"Step " + str(i + 1)), wx.VERTICAL )

                row_1_sizer = wx.BoxSizer( wx.HORIZONTAL )

                ctrl_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Control", wx.DefaultPosition, wx.DefaultSize, 0 )   
                ctrl_label.Wrap( -1 )
                
                row_1_sizer.Add( ctrl_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                ctrl_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.ctrl, wx.DefaultPosition, wx.DefaultSize, 0 )
                ctrl_txt.SetMinSize( wx.Size( 40,-1 ) )

                row_1_sizer.Add( ctrl_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                group_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Group", wx.DefaultPosition, wx.DefaultSize, 0 )
                group_label.Wrap( -1 )

                row_1_sizer.Add( group_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                group_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.group, wx.DefaultPosition, wx.DefaultSize, 0 )
                group_txt.SetMinSize( wx.Size( 40,-1 ) )

                row_1_sizer.Add( group_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                data_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Data", wx.DefaultPosition, wx.DefaultSize, 0 )
                data_label.Wrap( -1 )

                row_1_sizer.Add( data_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                data_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, " ".join(action.data), wx.DefaultPosition, wx.DefaultSize, 0 )
                row_1_sizer.Add( data_txt, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                ip_address_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"IP Address", wx.DefaultPosition, wx.DefaultSize, 0 )
                ip_address_label.Wrap( -1 )

                row_1_sizer.Add( ip_address_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                if action.command and action.command.ip_addresses:
                    ip_address_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.command.ip_addresses[0], wx.DefaultPosition, wx.DefaultSize, 0 )
                else:
                    ip_address_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
                row_1_sizer.Add( ip_address_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                up_btn1 = wx.BitmapButton( new_sizer.GetStaticBox(), wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

                up_btn1.SetBitmap( wx.Bitmap( self.resource_path(os.path.join("images", "up.bmp")), wx.BITMAP_TYPE_ANY ) )
                # up_btn1.SetBitmapCurrent( wx.NullBitmap )
                row_1_sizer.Add( up_btn1, 0, wx.ALL, 5 )
                up_btn1.Bind(wx.EVT_BUTTON, self.on_up)

                down_btn1 = wx.BitmapButton( new_sizer.GetStaticBox(), wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

                down_btn1.SetBitmap( wx.Bitmap( self.resource_path(os.path.join("images", "down.bmp")), wx.BITMAP_TYPE_ANY ) )
                row_1_sizer.Add( down_btn1, 0, wx.ALL, 5 )
                down_btn1.Bind(wx.EVT_BUTTON, self.on_down)

                add_btn1 = wx.BitmapButton( new_sizer.GetStaticBox(), wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

                add_btn1.SetBitmap( wx.Bitmap(self.resource_path(os.path.join("images", "add.bmp")), wx.BITMAP_TYPE_ANY ) )
                row_1_sizer.Add( add_btn1, 0, wx.ALL, 5 )
                add_btn1.Bind(wx.EVT_BUTTON, self.on_add)

                trash_btn1 = wx.BitmapButton( new_sizer.GetStaticBox(), wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW|0 )

                trash_btn1.SetBitmap( wx.Bitmap( self.resource_path(os.path.join("images", "trash.bmp")), wx.BITMAP_TYPE_ANY ) )
                row_1_sizer.Add( trash_btn1, 0, wx.ALL, 5 )
                trash_btn1.Bind(wx.EVT_BUTTON, self.on_delete)
                new_sizer.Add( row_1_sizer, 1, wx.EXPAND, 5 )

                row_2_sizer = wx.BoxSizer( wx.HORIZONTAL )

                name_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0 )
                name_label.Wrap( -1 )

                row_2_sizer.Add( name_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                name_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.name, wx.DefaultPosition, wx.DefaultSize, 0 )
                name_txt.SetMinSize( wx.Size( 200,-1 ) )

                row_2_sizer.Add( name_txt, 0, wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )


                command_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Command", wx.DefaultPosition, wx.DefaultSize, 0 )
                command_label.Wrap( -1 )

                row_2_sizer.Add( command_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                if action.command:
                    command_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, str(action.command.command), wx.DefaultPosition, wx.DefaultSize, 0 )
                else:
                    command_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
                command_txt.Enable( False )

                row_2_sizer.Add( command_txt, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )


                new_sizer.Add( row_2_sizer, 1, wx.EXPAND, 5 )

                staticline = wx.StaticLine( new_sizer.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
                new_sizer.Add( staticline, 0, wx.EXPAND |wx.ALL, 5 )

                row_3_sizer = wx.BoxSizer( wx.HORIZONTAL )


                # Add enable check
                enable_chk = wx.CheckBox(new_sizer.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0)
                row_3_sizer.Add(enable_chk, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT | wx.LEFT, 5)
                enable_chk.SetValue(bool(action.enabled))
                enable_chk.Bind(wx.EVT_CHECKBOX, self.on_update_chk)

                cal_msg_size_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"MsgSize", wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_msg_size_label.Wrap( -1 )

                row_3_sizer.Add( cal_msg_size_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                msgsize_cal_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.msg_size, wx.DefaultPosition, wx.DefaultSize, 0 )
                msgsize_cal_txt.Enable( False )
                msgsize_cal_txt.SetMinSize( wx.Size( 40,-1 ) )

                row_3_sizer.Add( msgsize_cal_txt, 0, wx.RIGHT|wx.LEFT|wx.ALIGN_CENTER_VERTICAL, 5 )

                cal_control_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Control", wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_control_label.Wrap( -1 )

                row_3_sizer.Add( cal_control_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                ctrl_cal_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.ctrl.zfill(2), wx.DefaultPosition, wx.DefaultSize, 0 )
                ctrl_cal_txt.Enable( False )
                ctrl_cal_txt.SetMinSize( wx.Size( 40,-1 ) )

                row_3_sizer.Add( ctrl_cal_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                cal_group_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Group", wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_group_label.Wrap( -1 )

                row_3_sizer.Add( cal_group_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                cal_group_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.group.zfill(2), wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_group_txt.Enable( False )
                cal_group_txt.SetMinSize( wx.Size( 40,-1 ) )

                row_3_sizer.Add( cal_group_txt, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                cal_data_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Data", wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_data_label.Wrap( -1 )

                row_3_sizer.Add( cal_data_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                cal_data_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, " ".join(action.data), wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_data_txt.Enable( False )

                row_3_sizer.Add( cal_data_txt, 1, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                cal_checksum_label = wx.StaticText( new_sizer.GetStaticBox(), wx.ID_ANY, u"Check Sum", wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_checksum_label.Wrap( -1 )

                row_3_sizer.Add( cal_checksum_label, 0, wx.ALIGN_CENTER_VERTICAL|wx.RIGHT|wx.LEFT, 5 )

                cal_checksum_txt = wx.TextCtrl( new_sizer.GetStaticBox(), wx.ID_ANY, action.check_sum, wx.DefaultPosition, wx.DefaultSize, 0 )
                cal_checksum_txt.Enable( False )
                cal_checksum_txt.SetMinSize( wx.Size( 40,-1 ) )

                row_3_sizer.Add( cal_checksum_txt, 0, wx.ALL, 5 )

                # Valid btn
                if action.valid:
                    valid_btn = wx.Button(new_sizer.GetStaticBox(), wx.ID_ANY, u"Test Command", wx.DefaultPosition, wx.DefaultSize, 0)
                    valid_btn.SetBackgroundColour('green')
                else:
                    valid_btn = wx.Button(new_sizer.GetStaticBox(), wx.ID_ANY, u"Invalid", wx.DefaultPosition, wx.DefaultSize, 0)
                    valid_btn.SetBackgroundColour('red')
                valid_btn.SetMinSize(wx.Size(100, -1))
                row_3_sizer.Add(valid_btn, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

                new_sizer.Add( row_3_sizer, 1, wx.EXPAND, 5 )



                


                # Connect Events
                name_txt.Bind( wx.EVT_TEXT, self.on_update_text )
                valid_btn.Bind(wx.EVT_BUTTON, self.on_test_action)
                ctrl_txt.Bind( wx.EVT_TEXT, self.on_update_text )
                group_txt.Bind( wx.EVT_TEXT, self.on_update_text )
                data_txt.Bind( wx.EVT_TEXT, self.on_update_text )
                ip_address_txt.Bind( wx.EVT_TEXT, self.on_update_text )



                new_panel.SetSizer(new_sizer)
                new_panel.Layout()
                action.panel = new_panel
                new_sizer.Fit(new_panel)
                self.step_sizer.Add(new_panel, 0, wx.EXPAND | wx.ALL, 5)


        self.m_scrolledWindow1.Fit()
        self.Layout()
        self.save_config()

    def refresh_all_actions(self):
        panels = self.get_panels()
        my_sizers = []
        for panel in panels:
            my_sizers.append(panel.GetChildren()[0])
        for sizer in my_sizers:
            children = sizer.GetChildren()
            index = int(sizer.GetLabel()[5:]) - 1
            self.action_list[index].create_command_from_text()
            if self.action_list[index].command_valid:
                children[2].SetLabel('Test Command')
                children[2].SetBackgroundColour("green")
            else:
                children[2].SetLabel('Invalid')
                children[2].SetBackgroundColour("red")

    def on_test_action(self, event):
        """Run a single action"""
        my_sizer = event.GetEventObject().GetParent()
        index = int(my_sizer.GetLabel()[5:]) - 1
        # print(self.action_list[index])
        if not self.action_list[index].valid:
            return
        if self.run_commands_btn.GetLabel() == 'Run Commands':
            if self.checking_cancel_btn.GetLabel() == "Stop Preview":
                self.on_cancel_checking()
            self.run_commands_btn.SetLabel('Cancel')
            dispatcher.send(signal="Abort", value=False)
            self.action_list[index].single_run = True
            self.control_thread.action_queue.put(self.action_list[index])
        else:
            return

    def on_update_text(self, event):
        """Updates self.action_list with changes to commands"""
        if not self.prefs.philips_testing:
            my_sizer = event.GetEventObject().GetParent()
            index = int(my_sizer.GetLabel().replace('Step ', '')) - 1
            self.action_list[index].text = event.GetEventObject().GetValue()
            self.action_list[index].update_text()

            # We need to update the valid button
            children = my_sizer.GetChildren()

            if self.action_list[index].valid:
                children[2].SetLabel('Test Command')
                children[2].SetBackgroundColour("green")
            else:
                children[2].SetLabel('Invalid')
                children[2].SetBackgroundColour("red")
            self.action_list[index].panel.Refresh()
            self.save_config()
        else:
            my_sizer = event.GetEventObject().GetParent()
            children = my_sizer.GetChildren()
            index = int(my_sizer.GetLabel().replace('Step ', '')) - 1

            # Get Values

            self.action_list[index].name = children[13].GetValue()
            self.action_list[index].ctrl = children[1].GetValue()
            self.action_list[index].group = children[3].GetValue()
            self.action_list[index].data = children[5].GetValue().split()
            self.action_list[index].ip_address = children[7].GetValue()
            self.action_list[index].update_text(philips=True)

            # Update status portion

            children[19].SetValue(self.action_list[index].msg_size.zfill(2))
            children[21].SetValue(self.action_list[index].ctrl.zfill(2))
            children[23].SetValue(self.action_list[index].group.zfill(2))
            children[25].SetValue(" ".join(self.action_list[index].data))
            children[27].SetValue(self.action_list[index].check_sum.zfill(2))



            # We need to update the valid button
            if self.action_list[index].valid:
                children[28].SetLabel('Test Command')
                children[28].SetBackgroundColour("green")
            else:
                children[28].SetLabel('Invalid')
                children[28].SetBackgroundColour("red")
            if self.action_list[index].command:
                children[15].SetValue(self.action_list[index].command.command)

            self.action_list[index].panel.Refresh()
            self.save_config()

    def on_update_chk(self, event):
        my_sizer = event.GetEventObject().GetParent()
        index = int(my_sizer.GetLabel().replace('Step ', '')) - 1
        self.action_list[index].enabled = event.GetEventObject().GetValue()
        self.save_config()

    def on_delete(self, event):
        """Deletes a panel and line item"""
        if len(self.action_list) == 1:
            return
        my_sizer = event.GetEventObject().GetParent()
        self.action_list.pop(int(my_sizer.GetLabel().replace('Step ', '')) - 1)
        self.refresh_actions()

    def on_up(self, event):
        my_sizer = event.GetEventObject().GetParent()
        oldindex = int(my_sizer.GetLabel().replace('Step ', '')) - 1
        newindex = oldindex - 1
        if oldindex <= 0:
            return
        self.action_list.insert(newindex, self.action_list.pop(oldindex))
        self.refresh_actions()
        self.action_list[newindex].panel.SetFocus()

    def on_down(self, event):
        my_sizer = event.GetEventObject().GetParent()
        oldindex = int(my_sizer.GetLabel().replace('Step ', '')) - 1
        if oldindex + 1 == len(self.action_list):
            return
        newindex = oldindex + 1
        self.action_list.insert(newindex, self.action_list.pop(oldindex))
        self.refresh_actions()
        self.action_list[newindex].panel.SetFocus()

    def on_add(self, event):
        my_sizer = event.GetEventObject().GetParent()
        index = int(my_sizer.GetLabel().replace('Step ', ''))
        action = self.action_list[index - 1]
        if not self.prefs.philips_testing:
            new_action = self.on_build_action(text=action.text)
        else:
            new_action = Action(enabled=action.enabled, text=action.text, name=action.name, group=action.group, ctrl=action.ctrl, data=action.data, ip_address=action.ip_address)
        self.action_list.insert(index, new_action)
        self.refresh_actions()
        self.action_list[index].panel.SetFocus()

    def on_num_loops(self, event):
        try:
            int(self.num_loops_txt.GetValue())
        except Exception:
            self.num_loops_txt.SetValue('1')
            self.num_loops_txt.SetInsertionPointEnd()

    def on_repeat(self, event):
        if self.repeat_chk.GetValue():
            self.num_loops_txt.Enable(False)
        else:
            self.num_loops_txt.Enable(True)

    def on_action_help(self, event):
        """Show some common actions"""
        dia = auto_test_gui.HelpWindow(self)
        dia.Show()

    def on_clear_debug(self, event):
        self.debug_txt.SetValue("")

    def on_save(self, event):
        save_file_dialog = wx.FileDialog(self,
                                         message='Select file to store commands to or create a new file',
                                         defaultDir=self.path,
                                         defaultFile="",
                                         wildcard="JSON files (*.json)|*.json",
                                         style=wx.FD_SAVE)
        if save_file_dialog.ShowModal() == wx.ID_OK:
            try:
                if not self.prefs.philips_testing:
                    save_list = [{"enabled": action.enabled, "text": action.text} for action in self.action_list]
                else:
                    save_list = [{"enabled": action.enabled, "text" : action.text, "name" : action.name, "group" : action.group, "ctrl" : action.ctrl, "data" : action.data, "ip_address" : action.ip_address} for action in self.action_list]
                with open(save_file_dialog.GetPath(), "w") as f:
                    json.dump(save_list, f, indent=4)
            except Exception as error:
                dia = wx.MessageDialog(self,
                                       caption="An Error occurred while saving file",
                                       message=f"Unable to save file\r{repr(error)}",
                                       style=wx.ICON_ERROR)
                dia.ShowModal()



    def on_load(self, event):
        open_file_dialog = wx.FileDialog(
            self, message="Open stored commands",
            defaultDir=self.path,
            defaultFile="",
            wildcard="JSON files (*.json)|*.json",
            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if open_file_dialog.ShowModal() == wx.ID_OK:
            try:
                with open(open_file_dialog.GetPath()) as f:
                    load_list = json.load(f)
                    self.action_list = []
                    for action in load_list:
                        if "name" in action:
                            
                            # This is a philips command
                            self.prefs.philips_testing = True
                            new_action = Action(enabled=action["enabled"], text=action["text"], name=action["name"], group=action["group"], ctrl=action["ctrl"], data=action["data"], ip_address=action["ip_address"])
                        else:
                            # Normal command
                            self.prefs.philips_testing = False
                            new_action = self.on_build_action(enabled=action["enabled"], text=action["text"])
                        self.action_list.append(new_action)
                    self.refresh_actions()
                self.save_config()
            except Exception as error:
                dia = wx.MessageDialog(self,
                                       caption="An Error occurred while loading file",
                                       message=f"Unable to load file\r{repr(error)}",
                                       style=wx.ICON_ERROR)
                dia.ShowModal()

        open_file_dialog.Destroy()

        

    def load_config(self):
        """Load Config"""
        try:

            with open("auto_test.pkl", 'rb') as f:
                pick = pickle.load(f)
            self.action_list = pick.action_list
            return pick
        except Exception as error:
            print('unable to load plk ', error)
            return Preferences()

    def save_config(self, event=None):
        """Update values in config file"""
        # Need to remove wx.Panel as it cannot be pkl'd
        temp_actions = []
        for action in self.action_list:
            temp_actions.append(Action(enabled=action.enabled, text=action.text, name=action.name, group=action.group, ctrl=action.ctrl, data=action.data, ip_address=action.ip_address))
        self.prefs.action_list = temp_actions
        with open("auto_test.pkl", "wb") as f:
            pickle.dump(self.prefs, f)

        # print(self.prefs.__dict__)

    def resource_path(self, relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            base_path = sys._MEIPASS
        except Exception:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def update_required(self, sender, message, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)


def main():
    """run the main program"""
    at_app = wx.App()  # redirect=True, filename="log.txt")
    # do processing/initialization here and create main window
    at_frame = Auto_Test_Frame(None)
    at_frame.Show()
    at_app.MainLoop()


if __name__ == '__main__':
    main()
